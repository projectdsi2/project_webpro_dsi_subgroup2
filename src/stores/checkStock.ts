import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { CheckStock} from '@/types/CheckStock'
import { useCheckStockDetailStore } from './checkStockDetail'
import { useAuthStore } from './auth'
import type { CheckStockDetail } from '@/types/CheckStockDetail'
import { useMaterialStore } from './material'

export const useCheckStockStore = defineStore('checkStock', () => {
    const checkStockDetailStore = useCheckStockDetailStore()
    const authStore = useAuthStore()
    const checkStockDialog = ref(false)

    function newCheckStock() {

      checkStockDialog.value=true
    }
  
    const checkStockData = ref<CheckStock[]>([
      {id:1,user:'phongphak kajornchaiyakul',date:'2024-01-01',time:'00:00 น.',details:checkStockDetailStore.checkStockDetailData1},
      {id:2,user:'wanwadee noijaroen',date:'2024-01-02',time:'01:00 น.',details:checkStockDetailStore.checkStockDetailData2},
      {id:3,user:'wantania phonsomret',date:'2024-01-03',time:'02:00 น.',details:checkStockDetailStore.checkStockDetailData3}

    ])
    
    


    const createCheckStock= ((dmy:string,hm:string)=>{

      const newCheckStockReceipt = ref<CheckStock>({
        id: 1,
        user: '',
        date: '',
        time: '',
        details: []
      })
      console.log(newCheckStockReceipt.value)
      newCheckStockReceipt.value.id = checkStockData.value[checkStockData.value.length-1].id+1
      newCheckStockReceipt.value.user = authStore.userLogin.currentUser.fullName
      newCheckStockReceipt.value.date=dmy
      newCheckStockReceipt.value.time=hm
      // newCheckStockReceipt.value.date = DateMonthYear.value
      // newCheckStockReceipt.value.time = HourMinute.value
      // checkStockData.value.push(newCheckStockReceipt.value)
      console.log(newCheckStockReceipt.value)
      checkStockData.value.push(newCheckStockReceipt.value)
    })
    // const addDetail= ((details:CheckStockDetail[])=>{
    //   newCheckStockReceipt.value.details = details
    // })

    const setDetail = (details:CheckStockDetail[]) => {
      checkStockData.value[checkStockData.value.length-1].details = details
    }
    const delLast = ref(false)

    const materialStore = useMaterialStore()

    watch(delLast,(newValue) => {
      const rollBackQty = ref<number[]>([])
      for(const item of checkStockData.value[checkStockData.value.length-1].details){
        rollBackQty.value.push(item.checkLast)
      }
      materialStore.updateQty(rollBackQty.value)
      delLast.value = false
    })

    const confirmDelete = () =>{
      delLast.value = true
    }
    
    
  return {  checkStockData,checkStockDialog,newCheckStock,createCheckStock,setDetail,confirmDelete}
})
