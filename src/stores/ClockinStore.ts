import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { ClockInOut } from '@/types/ClockinType'
import { useAuthStore } from './auth'
import { useUserStore } from './user'

export const useClockInOutStore = defineStore('clockinout', () => {
    const authStore = useAuthStore()
    const userStore = useUserStore
    const clockinoutData = ref<ClockInOut[]>(
        [
            {
                ID: 1,
                userId: 1,
                fullname: 'sirapop wongtim',
                clockIn: '10/01/2024 09:00:00',
                clockOut: '17:00:00',
                totalHours: '8hrs 0min',
                IN :0,
                OUT:0,
            },
            {
                ID: 2,
                userId: 1,
                fullname: 'sirapop wongtim',
                clockIn: '11/01/2024 10:00:00',
                clockOut: '11/01/2024 17:00:00',
                totalHours: '7hrs 0min',
                IN :0,
                OUT:0,
            },
            {
                ID: 3,
                userId: 1,
                fullname: 'sirapop wongtim',
                clockIn: '12/01/2024 09:00:00',
                clockOut: '12/01/2024 20:00:00',
                totalHours: '11hrs 0min',
                IN :0,
                OUT:0,
            },
            {
                ID: 4,
                userId: 2,
                fullname: 'phongphak kajornchaiyakul',
                clockIn: '13/01/2024 09:00:00',
                clockOut: '13/01/2024 20:00:00',
                totalHours: '11hrs 0min',
                IN :0,
                OUT:0,
            },
            {
                ID: 5,
                userId: 2,
                fullname: 'phongphak kajornchaiyakul',
                clockIn: '14/01/2024 09:00:00',
                clockOut: '14/01/2024 18:00:00',
                totalHours: '9hrs 0min',
                IN :0,
                OUT:0,
            },
            {
                ID: 6,
                userId: 2,
                fullname: 'phongphak kajornchaiyakul',
                clockIn: '15/01/2024 08:00:00',
                clockOut: '15/01/2024 16:00:00',
                totalHours: '8hrs 0min',
                IN :0,
                OUT:0,
            },
            {
                ID: 7,
                userId: 3,
                fullname: 'wanwadee noijaroen',
                clockIn: '12/01/2024 09:00:00',
                clockOut: '12/01/2024 18:00:00',
                totalHours: '8hrs 0min',
                IN :0,
                OUT:0,
            },
            {
                ID: 8,
                userId: 3,
                fullname: 'wanwadee noijaroen',
                clockIn: '13/01/2024 10:00:00',
                clockOut: '13/01/2024 18:00:00',
                totalHours: '8hrs 0min',
                IN :0,
                OUT:0,
            },
            {
                ID: 9,
                userId: 3,
                fullname: 'wanwadee noijaroen',
                clockIn: '14/01/2024 09:00:00',
                clockOut: '14/01/2024 20:00:00',
                totalHours: '11hrs 0min',
                IN :0,
                OUT:0,
            },
            {
                ID: 10,
                userId: 4,
                fullname: 'wantania phonsomret',
                clockIn: '12/01/2024 09:00:00',
                clockOut: '12/01/2024 17:00:00',
                totalHours: '8hrs 0min',
                IN :0,
                OUT:0,
            },
            {
                ID: 11,
                userId: 4,
                fullname: 'wantania phonsomret',
                clockIn: '13/01/2024 09:00:00',
                clockOut: '13/01/2024 17:00:00',
                totalHours: '8hrs 0min',
                IN :0,
                OUT:0,
            },
            {
                ID: 12,
                userId: 4,
                fullname: 'wantania phonsomret',
                clockIn: '14/01/2024 09:00:00',
                clockOut: '14/01/2024 17:00:00',
                totalHours: '8hrs 0min',
                IN :0,
                OUT:0,
            },
            {
                ID: 13,
                userId: 5,
                fullname: 'user 1',
                clockIn: '13/01/2024 09:00:00',
                clockOut: '13/01/2024 17:00:00',
                totalHours: '8hrs 0min',
                IN :0,
                OUT:0,
            },
            {
                ID: 14,
                userId: 5,
                fullname: 'user 1',
                clockIn: '14/01/2024 09:00:00',
                clockOut: '14/01/2024 17:00:00',
                totalHours: '8hrs 0min',
                IN :0,
                OUT:0,
            },
            {
                ID: 15,
                userId: 5,
                fullname: 'user 1',
                clockIn: '15/01/2024 09:00:00',
                clockOut: '15/01/2024 17:00:00',
                totalHours: '8hrs 0min',
                IN :0,
                OUT:0,
            },

        ])

        function formatTime(date: Date): string {
            const day = date.getDate().toString().padStart(2, '0')
            const month = date.getMonth().toString().padStart(2, '') + 1
            const year = date.getFullYear().toString()
            const hours = date.getHours().toString().padStart(2, '0')
            const minutes = date.getMinutes().toString().padStart(2, '0')
            const seconds = date.getSeconds().toString().padStart(2, '0')
            return `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`
          }
        

    function formatTotalTime(totalTime: number): string {
        const hours = Math.floor(totalTime)
        const minutes = Math.round((totalTime % 1) * 60)
        return `${hours}hrs ${minutes}min `
      }

    const lastId = ref(15)
    function clockIn() {
        const entry: ClockInOut = {
            ID: lastId.value++,
            userId: authStore.userLogin.currentUser.id,
            fullname: authStore.userLogin.currentUser.fullName,
            clockIn: formatTime(new Date()),
            clockOut: '',
            totalHours: '0hrs 0min',
            IN : new Date().getTime(),
            OUT : 0,
        };
        clockinoutData.value.push(entry)
        lastId.value++
    }

    function clockOut(clock: ClockInOut) {
        const lastEntryIndex = clockinoutData.value.findIndex((item) => item === clock);
        if (lastEntryIndex !== -1) {
            clockinoutData.value[lastEntryIndex].clockOut = formatTime(new Date());
            clockinoutData.value[lastEntryIndex].OUT = new Date().getTime()
            const timeDiff = clockinoutData.value[lastEntryIndex].OUT - clockinoutData.value[lastEntryIndex].IN
            
            const totalTime = timeDiff / (1000 * 60 * 60)
            if (!isNaN(totalTime) && totalTime >= 0) {
                clockinoutData.value[lastEntryIndex].totalHours = formatTotalTime(totalTime)
                console.log('Clockout works')
            } else {
                clockinoutData.value[lastEntryIndex].totalHours = '0hrs 0min';
                console.log('Clockout doesnt work')
            }
        }
    }
    

    return { clockinoutData, clockIn, lastId, clockOut,formatTotalTime}
})