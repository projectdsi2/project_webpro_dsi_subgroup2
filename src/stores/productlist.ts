import { type Product } from "@/types/Product"

const products1: Product[] = [
    {   id:1,
        name:'ลาเต้',
        price: 35.00,
        type: 1,
        subCategory: 0
    },
    {   id:2,
        name:'กาแฟ',
        price: 35.00,
        type: 1,
        subCategory: 0
    },
    {   id:3,
        name:'โกโก้',
        price: 35.00,
        type: 1,
        subCategory: 0
    },
    {   id:4,
        name:'เอสเปรสโซ่',
        price: 35.00,
        type: 1,
        subCategory: 0
    },
    {   id:5,
        name:'นมชมพู',
        price: 35.00,
        type: 1,
        subCategory: 0
    },
    {   id:6,
        name:'นมสด',
        price: 35.00,
        type: 1,
        subCategory: 0
    },
    {   id:7,
        name:'นมกล้วย',
        price: 35.00,
        type: 1,
        subCategory: 0
    },
    {   id:8,
        name:'นมมินต์',
        price: 35.00,
        type: 1,
        subCategory: 0
    },
    {   id:9,
        name:'คาปูชินโน่',
        price: 35.00,
        type: 1,
        subCategory: 0
    }
  ]
  
  const products2: Product[] = [
    {   id:1,
        name:'ชามะลิ',
        price: 35.00,
        type: 2,
        subCategory: 0
    },
    {   id:2,
        name:'ชาเขียว',
        price: 35.00,
        type: 2,
        subCategory: 0
    },
    {   id:3,
        name:'ชาไทย',
        price: 35.00,
        type: 2,
        subCategory: 0
    },
    {   id:4,
        name:'ชาดำ',
        price: 35.00,
        type: 2,
        subCategory: 0
    }
  ]
  
  const products3: Product[] = [
    {   id:1,
        name:'บราวนี่',
        price: 35.00,
        type: 3,
        subCategory: 0
    },
    {   id:2,
        name:'บราวนั่น',
        price: 35.00,
        type: 3,
        subCategory: 0
    },
    {   id:3,
        name:'บราวโน่น',
        price: 35.00,
        type: 3,
        subCategory: 0
    }
  ]

  export {products1, products2, products3}
