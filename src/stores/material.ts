import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Material } from '@/types/Material'

export const useMaterialStore = defineStore('material', () => {

  const materials = ref<Material[]>([
    {
        id: 1,
        Name: 'Coffee Beans',
        Price: 400.0,
        Qty: 200,
        Min: 20,
        Unit: 'kilogram',
        
    },
    {
        id: 2,
        Name: 'GreenTea Powder',
        Price: 150.0,
        Qty: 100,
        Min: 20,
        Unit: 'kilogram',
        
    },
    {
        id: 3,
        Name: 'Syrup',
        Price: 90.0,
        Qty: 135,
        Min: 20,
        Unit: 'Bottle'
        
    },
    {
        id: 4,
        Name: 'Sugar',
        Price: 60.0,
        Qty: 100,
        Min: 20,
        Unit: 'kilogram',
        
    },
    {
        id: 5,
        Name: 'ThaiTea Powder',
        Price: 120.0,
        Qty: 200,
        Min: 20,
        Unit: 'kilogram',
        
    },
    {
        id: 6,
        Name: 'Cocoa',
        Price: 360.0,
        Qty: 200,
        Min: 20,
        Unit: 'kilogram',
        
    }
  ])

  const updateQty =(qtys:number[]) => {
    for (const [index, item] of materials.value.entries()) {
        item.Qty = qtys[index]
    }
  }

  return {materials,updateQty}
})
