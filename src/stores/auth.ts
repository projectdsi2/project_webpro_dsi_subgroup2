import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { defaultUser, users, type User, type Role } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
  const userLogin = {
    currentUser: ref(defaultUser),
    users: ref(users)
  }

  const methods = {
    authenticateUser(): boolean {
      return userLogin.currentUser.value.id > -1;
    },
    grantAccess(routeRoles: Role[]): boolean {
      return routeRoles.every(role => userLogin.currentUser.value.role.includes(role));
    },
    setCurrentUser(newUser: User): void {
      userLogin.currentUser.value = newUser;
      const parsed = JSON.stringify(newUser);
      localStorage.setItem('user', parsed);
    },
    checkUser(username: string, password: string): boolean {
      const foundUser = userLogin.users.value.find(user => user.username === username && user.password === password);

      if (foundUser) {
        this.setCurrentUser(foundUser);
        return true;
      }
      return false;
    },
    getCurrentUser(): User {
      return { ...userLogin.currentUser.value };
    },
    clearUser() {
      localStorage.removeItem('user');
    }
  };

  return { userLogin, methods };
});