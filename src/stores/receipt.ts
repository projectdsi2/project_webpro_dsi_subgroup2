import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'

export const useReceiptStore = defineStore('receipt', () => {
  const receiptItems = ref<ReceiptItem[]>([])
  const authStore = useAuthStore()
  const receiptDialog = ref(false)
  const point = ref(0)
  const receiveMoney = ref(0)

  

  const receipt = ref<Receipt>({
    id: 0,
    createDate: new Date(),
    createdTimestamp: computed(() => new Date().getTime()).value,
    totalBefore: 0,
    memberDiscount: 0,
    total: 0,
    receiveAmount: 0,
    change: 0,
    paymentType: 'cash',
    userId: 0,
    memberId: 0
  })

  function showReceiptDialog() {
    receipt.value.receiptItems=receiptItems.value
    receiptDialog.value=true
  }
function clear(){
    point.value = 0
    receiptItems.value = []
    receiveMoney.value = 0
    receipt.value = {
        id: 0,
        createDate: new Date(),
        createdTimestamp: computed(() => new Date().getTime()).value,
        totalBefore: 0,
        memberDiscount: 0,
        total: 0,
        receiveAmount: 0,
        change: 0,
        paymentType: 'Cash',
        userId: authStore.userLogin.currentUser.id,
        user: authStore.userLogin.currentUser,
        memberId: 0
      }
}


  function addReceiptItem(product: Product) {
    point.value++
    let subCategory = 'Cold'
    if (product.subCategory === 2) (subCategory = 'Hot'), calReceipt()
    else if (product.subCategory === 3) (subCategory = 'Frappe'), calReceipt()

    if (product.subCategory === 2) calReceipt()

    if (product.type === 3) (subCategory = '-'), calReceipt()
    const index = receiptItems.value.findIndex(
      (item) =>
        item.product?.id === product.id &&
        item.product.type === product.type &&
        item.slcSubCate === product.subCategory
    )

    if (index >= 0) {
      receiptItems.value[index].unit++
      calReceipt()
      return
    }

    const newReceipt: ReceiptItem = {
      id: -1,
      name: product.name,
      subCategory: subCategory,
      price: product.price,
      unit: 1,
      productId: product.id,
      product: product,
      slcSubCate: product.subCategory,
    }
    receiptItems.value.push(newReceipt)
    calReceipt()
  }


  function removeReceiptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    const removePoint = receiptItem.unit
    point.value = point.value-removePoint
    receiptItems.value.splice(index, 1)
    calReceipt()
  }

  function inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
  }
  function dec(item: ReceiptItem) {
    if (item.unit === 1) {
      removeReceiptItem(item)
    }
    item.unit--
    calReceipt()
  }
  const updateTotal = () => {
    receipt.value.total = receipt.value.memberDiscount + (receipt.value.totalBefore || 0);
   }
  function calReceipt() {
    let totalBefore = 0
    for (const item of receiptItems.value) {
      totalBefore = totalBefore + item.price * item.unit
    }
    receipt.value.totalBefore = totalBefore
    updateTotal()
    calculateChange()
    console.log(receipt.value.change)
    
  }

  const calculateChange = () => {
  if (receipt.value.paymentType === 'Promptpay' ) {
    receipt.value.change = 0;
    receiveMoney.value = 0
    console.log("ว่าง ๆ",receipt.value.paymentType)
    console.log("ว่าง ๆ",receipt.value.change)
  } else if(receipt.value.paymentType === 'Cash') {
    receipt.value.change = receiveMoney.value - (receipt?.value.totalBefore || 0);
    console.log("ว่าง ๆ",receipt.value.paymentType)
  }
};
  

  return {
    receiptItems,
    receipt,
    receiptDialog,
    point,
    receiveMoney,
    showReceiptDialog,
    addReceiptItem,
    removeReceiptItem,
    inc,
    dec,
    calReceipt,
    clear,

  }
})
