import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { CheckStockDetail } from '@/types/CheckStockDetail'
import type { CheckStock } from '@/types/CheckStock'
import { useCheckStockStore } from './checkStock'
import { useMaterialStore } from './material'

export const useCheckStockDetailStore = defineStore('checkStockDetail', () => {
  const checkStockStore = useCheckStockStore()
  const materialStore = useMaterialStore()

  const checkStockDetailData1 = ref<CheckStockDetail[]>([
    {
      id: 1,
      checkStockId: 1,
      materialId: 1,
      material: 'Coffee Beans',
      checkLast: 1,
      checkRemain: 2
    },
    {
      id: 2,
      checkStockId: 1,
      materialId: 2,
      material: 'GreenTea Powder',
      checkLast: 2,
      checkRemain: 3
    },
    { id: 3, checkStockId: 1, materialId: 3, material: 'Syrup', checkLast: 3, checkRemain: 4 },
    { id: 4, checkStockId: 1, materialId: 4, material: 'Sugar', checkLast: 4, checkRemain: 5 },
    {
      id: 5,
      checkStockId: 1,
      materialId: 5,
      material: 'ThaiTea Powder',
      checkLast: 5,
      checkRemain: 6
    },
    { id: 6, checkStockId: 1, materialId: 6, material: 'Cocoa', checkLast: 6, checkRemain: 7 }
  ])
  const checkStockDetailData2 = ref<CheckStockDetail[]>([
    {
      id: 1,
      checkStockId: 2,
      materialId: 1,
      material: 'Coffee Beans',
      checkLast: 1,
      checkRemain: 3
    },
    {
      id: 2,
      checkStockId: 2,
      materialId: 2,
      material: 'GreenTea Powder',
      checkLast: 2,
      checkRemain: 4
    },
    { id: 3, checkStockId: 2, materialId: 3, material: 'Syrup', checkLast: 3, checkRemain: 5 },
    { id: 4, checkStockId: 2, materialId: 4, material: 'Sugar', checkLast: 4, checkRemain: 6 },
    {
      id: 5,
      checkStockId: 2,
      materialId: 5,
      material: 'ThaiTea Powder',
      checkLast: 5,
      checkRemain: 7
    },
    { id: 6, checkStockId: 2, materialId: 6, material: 'Cocoa', checkLast: 6, checkRemain: 8 }
  ])
  const checkStockDetailData3 = ref<CheckStockDetail[]>([
    {
      id: 1,
      checkStockId: 3,
      materialId: 1,
      material: 'Coffee Beans',
      checkLast: 1,
      checkRemain: 200
    },
    {
      id: 2,
      checkStockId: 3,
      materialId: 2,
      material: 'GreenTea Powder',
      checkLast: 2,
      checkRemain: 100
    },
    { id: 3, checkStockId: 3, materialId: 3, material: 'Syrup', checkLast: 3, checkRemain: 135 },
    { id: 4, checkStockId: 3, materialId: 4, material: 'Sugar', checkLast: 4, checkRemain: 100 },
    {
      id: 5,
      checkStockId: 3,
      materialId: 5,
      material: 'ThaiTea Powder',
      checkLast: 5,
      checkRemain: 200
    },
    { id: 6, checkStockId: 3, materialId: 6, material: 'Cocoa', checkLast: 6, checkRemain: 200 }
  ])

  const checkStockDetailData = ref<CheckStockDetail[]>([])
  const newCheckStockDetails = ref<CheckStockDetail[]>([])

  const getDetailData = () => {
    const sendCheckDetail = ref<CheckStockDetail[]>([])
    sendCheckDetail.value = newCheckStockDetails.value
    newCheckStockDetails.value = []
    return sendCheckDetail
  }

  const createCheckStockDetail = (index: number, chkId: number, chkRemain: number) => {
    const latestId =
      checkStockDetailData.value.length > 0
        ? Math.max(...checkStockDetailData.value.map((detail) => detail.id))
        : 0

    const newCheckStockDetail = ref<CheckStockDetail>({
      id: 0,
      checkStockId: 0,
      materialId: 0,
      material: '',
      checkLast: 0,
      checkRemain: 0
    })
    console.log('ดีเทล')
    newCheckStockDetail.value.id = latestId + 1
    newCheckStockDetail.value.checkStockId = chkId
    newCheckStockDetail.value.materialId = materialStore.materials[index].id
    newCheckStockDetail.value.material = materialStore.materials[index].Name
    newCheckStockDetail.value.checkLast = materialStore.materials[index].Qty
    newCheckStockDetail.value.checkRemain = chkRemain
    newCheckStockDetails.value.push(newCheckStockDetail.value)
    checkStockDetailData.value.push(newCheckStockDetail.value)
  }

  return {
    checkStockDetailData1,
    checkStockDetailData2,
    checkStockDetailData3,
    createCheckStockDetail,
    checkStockDetailData,
    getDetailData
  }
})
