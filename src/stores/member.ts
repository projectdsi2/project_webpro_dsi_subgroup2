import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Customer } from '@/types/Customer'
  
export const useMemberStore = defineStore('member', () => {
    const members = ref<Customer[]>( [
      {
        ID: 1,
        Firstname: 'Rick',
        Lastname: 'Smith',
        Tel: '0855122249',
        Point: 10,
      },
      {
        ID: 2,
        Firstname: 'Morty',
        Lastname: 'Smith',
        Tel: '0862475512',
        Point: 0,
      },
      {
        ID: 3,
        Firstname: 'Cloud',
        Lastname: 'Strife',
        Tel: '0877655312',
        Point: 0,
      },
    ])

  
  const currentMember = ref<Customer|null>()
    const searchMember = (tel: string) => {
        const index = members.value.findIndex((item) => item.Tel === tel)
        if(index<0){
            currentMember.value = null
        }
        currentMember.value = members.value[index]
    }

    const updatePoint = (newPoint: number) => {
      if (currentMember.value) {
        currentMember.value.Point = newPoint;
      }
    };
  
    function clear() {
        currentMember.value = null
      }
      
    const memberDialog = ref(false)
    const dialogDelete = ref(false)
    const editIndex = -1
    function newCustomer() {
        memberDialog.value=true
      }
    
      const createNewMember = (newMember:Customer)=>{
        members.value.push(newMember)
      }
    
      const initialCustomer: Customer = {
        ID: -1,
        Firstname: 'Rick',
        Lastname: 'Astley',
        Tel: '0000000000',
        Point: 0,
       }

      const editedCustomer = ref<Customer>(
      JSON.parse(JSON.stringify(initialCustomer))
    );

    const initCustomer = ()=>{
        members.value = [
          {
            ID: 1,
            Firstname: 'Rick',
            Lastname: 'Smith',
            Tel: '0855122249',
            Point: 10,
          },
          {
            ID: 2,
            Firstname: 'Morty',
            Lastname: 'Smith',
            Tel: '0862475512',
            Point: 1,
          },
          {
            ID: 3,
            Firstname: 'Cloud',
            Lastname: 'Strife',
            Tel: '0877655312',
            Point: 0,
          },
        ];
      
      
    }

    return{ 
        members, currentMember,memberDialog,editedCustomer,editIndex,dialogDelete,
        initCustomer,searchMember, clear,updatePoint,newCustomer,createNewMember
    }
})

