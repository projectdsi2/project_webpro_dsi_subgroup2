type ClockInOut = {
    ID: number
    userId : number
    fullname : string
    clockIn: string
    clockOut: string
    totalHours: string
    IN :number
    OUT : number
  }

export type {ClockInOut}
