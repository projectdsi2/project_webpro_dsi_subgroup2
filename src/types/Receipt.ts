import type {User} from './User'
import type {Customer} from './Customer'
import type { ReceiptItem } from './ReceiptItem';

type Receipt = {
    id: number;
    createDate: Date;
    createdTimestamp: number    
    totalBefore:number,
    memberDiscount:number,
    total: number;
    receiveAmount: number;
    change: number;
    paymentType: string;
    userId: number;
    user?: User;
    memberId: number;
    member?:Customer
    receiptItems?:ReceiptItem[]
}

export type {Receipt}
