type Material = {
    id: number
    Name: string
    Price: number
    Qty: number
    Min: number
    Unit: string
  }

  export type {Material}