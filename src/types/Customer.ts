type Customer = {
    ID: number;
    Firstname: string;
    Lastname: string;
    Tel: string;
    Point: number;
    deleted?: boolean;
  };

export type {Customer}

