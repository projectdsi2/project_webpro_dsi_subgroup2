type Product = {
    id: number;
    name: string;
    price: number;
    type: number;
    subCategory: number
}

export{type Product}