type Role = 'manager' | 'employee';
type Type = 'part-time' | 'fulltime'
type workTime = '9:00-17:00' | '9:00-18:00' | '8:00-17:00'


interface User {
    id: number;
    username: string;
    password: string;
    fullName: string;
    role: Role[]; // manager, employee
    type: Type[]; // part-time, fulltime
    workTime: workTime ;
    wage: string;
    salary: string;
}

const users: User[] = [
    {
        id: 1,
        username: 'อ๋าซอจุน',
        password: 'sirapop123',
        fullName: 'sirapop wongtim',
        role: ['manager'],
        type: ['fulltime'],
        workTime: '9:00-17:00',
        wage: '1500',
        salary: '100000'
    },
    {
        id: 2,
        username: 'pelinlnwZaa007',
        password: 'phongphak123',
        fullName: 'phongphak kajornchaiyakul',
        role: ['employee'],
        type: ['part-time'],
        workTime: '9:00-18:00',
        wage: '100',
        salary: '10000'
    },
    {
        id: 3,
        username: 'jiffy2003',
        password: 'wanwadee123',
        fullName: 'wanwadee noijaroen',
        role: ['manager'],
        type: ['fulltime'],
        workTime: '9:00-18:00',
        wage: '100',
        salary: '10000'
    },
    {
        id: 4,
        username: 'KeyBread4444',
        password: 'wantania123',
        fullName: 'wantania phonsomret',
        role: ['employee'],
        type: ['fulltime'],
        workTime: '9:00-18:00',
        wage: '100',
        salary: '10000'
    },
    {
        id: 5,
        username: 'User1',
        password: 'user1',
        fullName: 'user 1',
        role: ['employee'],
        type: ['fulltime'],
        workTime: '9:00-18:00',
        wage: '100',
        salary: '10000'
    }
];

const defaultUser: User = {
    id: 5,
    username: 'User1',
    password: 'user1',
    fullName: 'user 1',
    role: ['employee'],
    type: ['fulltime'],
    workTime: '9:00-18:00',
    wage: '100',
    salary: '10000'
};

export { users, defaultUser, type Role, type User }